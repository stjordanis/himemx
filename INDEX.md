# HIMEMX

HimemX is a XMS memory manager derived from FreeDOS Himem.


## Contributions

NLS specific corrections, updates and submissions should not be 
directly to submitted this project. NLS is maintained at the [FD-NLS](https://github.com/shidel/fd-nls)
project on GitHub. If the project is still actively maintained by it's
developer, it may be beneficial to also submit changes to them directly.

## HIMEMX.LSM

<table>
<tr><td>title</td><td>HIMEMX</td></tr>
<tr><td>version</td><td>3.36</td></tr>
<tr><td>entered&nbsp;date</td><td>2021-01-08</td></tr>
<tr><td>description</td><td>HimemX is a XMS memory manager derived from FreeDOS Himem.</td></tr>
<tr><td>keywords</td><td>XMS, himem, memory manager</td></tr>
<tr><td>author</td><td>Andreas "Japheth" Grech, Tom Ehlert, Till Gerken</td></tr>
<tr><td>maintained&nbsp;by</td><td>Mercury Thirteen (mercury0x0d@protonmail.com)</td></tr>
<tr><td>primary&nbsp;site</td><td>https://github.com/Baron-von-Riedesel/HimemX</td></tr>
<tr><td>alternate&nbsp;site</td><td>https://github.com/FDOS/himemX</td></tr>
<tr><td>original&nbsp;site</td><td>http://himemx.sourceforge.net/</td></tr>
<tr><td>platforms</td><td>DOS, FreeDOS</td></tr>
<tr><td>copying&nbsp;policy</td><td>[General Public License, Artistic license and Public Domain](LICENSE)</td></tr>
</table>
